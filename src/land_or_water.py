#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File              : map_locations.py
# Author            : Gutama Ibrahim Mohammd <gutama.mohammad.uib.no>
# Date              : 21.06.2022
# Last Modified Date: 21.06.2022
# Last Modified By  : Gutama Ibrahim Mohammad <gutama.mohammad@uib.no>


import numpy as np 
from matplotlib import colors
import matplotlib.pyplot as plt
import sys

def convert_geographic_location_to_coordinate(location="45N 912E"):
    latitude,longitude = location.split(" ")
    latitude =int(latitude[:-1])
    longitude=int(longitude[:-1])
    x =int(((90 -latitude)/180) * 21600)
    y=int(((180 +longitude)/360)*43200)
    return x,y
def get_pixel_value(data,position):
    return data[position]


landcover_type_encoding ={0:"water", 1:"Evergreen Needleleaf Forest",2:"Evergreen Broadleaf Forest",
                          3:"Deciduous Needleleaf Forest", 4:"Deciduous Broadleaf Forest",5:"Mixed Forest",
                          6:"Woodland", 7:"Wooded Grassland", 8:"Closed Shrubland" ,9:"Open Shrubland",
                          10:"Grassland",11:"Grobland",12:"Bare Ground",12:"Urban and Build"}

def location_classifier(location):
    data = np.fromfile("./data/gl-latlong-1km-landcover.bsq",dtype=np.uint8,)
    data = data.reshape((21600,43200))
    position = convert_geographic_location_to_coordinate(location)
    pixel_value=get_pixel_value(data=data,position=position)
    print(f'{location} is on {landcover_type_encoding[pixel_value]}')

      

if __name__ =="__main__":
    location = input("Please, input the geographic location you want to classify (e.g 48.95N 9.13E))\n")
    location_classifier(location)


